(function () {
  Polymer({
    is: 'my-list',
    // properties: {
    //   items: {
    //     type: Array,
    //     notify: true,
    //   }
    // },
    ready: function() {
      for(item in workItems) {
        workItems[item]["url"] = "/items/" + workItems[item]["id"];
        workItems[item]["image_card_path"] = "images/items/" + workItems[item]["image_card"];
      }
      this.items = workItems;
    }
  });
})();

