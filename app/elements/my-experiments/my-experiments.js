var experiments = [
  {
    link: "jeremy/index.html",
    linkText: "Jeremy Scroll",
    description: "This dude that I work with, Jeremy, is a UX designer. He really hates default scrollbars and generally prefers a custom solution. So I made this jquery plugin, $.jeremyScroll(), for him so that we can easily attach custom scrollbars. You can specify a bunch of characteristics (like the image for the shuttle). But obviously you should just stick with Jeremy's image."
  },
  {
    link: "color-shift/index.html",
    linkText: "Color Shift",
    description: "This one is another jquery plugin that I did as an experiment to try to get colors to blur from one to the next. You can specify the attribute that you want to change (text-color, background-color, border-color), the color you want it to change to and the time you want it to take to get there. I guess this could be used for an effect when a user hovers over a link if the color difference is subtle. If you get too crazy with it, it can look pretty ugly though. And, not gonna lie, it's still a little buggy. And yes, I know that you can do this with CSS3 transitions."
  },
  {
    link: "rotate/public/index.html",
    linkText: "Rotate",
    description: "An experiment that I'm using to learn how to click and drag to rotate things. I'm calculating the angle of the mouse in relation to the center of the image and rotating based on that. It's also maintaining the state of the image so if you let go of the mouse button and then click/drag somewhere else on the page it can adjust accordingly. When I'm finished it should be able to rotate on all three axes. Update: Can now rotate on all three axes."
  },
  {
    link: "holiday-card/index.html",
    linkText: "Holiday Factory",
    description: "These are some experiments that I did when doing research for our holiday card project at work. It mostly involves css transforms in both 2D and 3D and changing them in javascript based on percentages. These were really fun to work on."
  },
  {
    link: "rectangles/index.html",
    linkText: "Rectangles everywhere!",
    description: "I don't know why, just draw some rectangles. It looks pretty cool if you draw a whole bunch of them. I wrote it as a jQuery plugin so it's pretty easy to install. Click and drag to draw them. You can change the color with the color picker. Click \"+\"\" or \"-\" to change the border size. Click the \"x\" in the individual rectangle to erase it or click \"Clear all\" to erase or all of them. Make sure you click \"Enable\" or \"Disable\" to start and stop. I used Spectrum Colorpicker, written by Brian Grinstead. You can find it at https://github.com/bgrins/spectrum"
  },
  {
    link: "pan/index.html",
    linkText: "Explore Hyrule.",
    description: "This is something I started messing around with back when parallax was the cool new thing. I figured that if you could make some elements move relative to each other based on scroll then you could leverage that to do some other interesting things. Did it really turn out all that interesting? Kinda, I guess. Scroll down to pan around the map."
  },
  {
    link: "zoom/index.html",
    linkText: "Zoom!",
    description: "More scroll-based shenanigans! Scroll down to zoom in on images and even html text. \"Traveling through hyperspace ain't like dusting crops, boy!\""
  }
];

(function () {
  Polymer({
    is: 'my-experiments',
    ready: function(){
      console.log('experiments')
      this.experiments = experiments;
    }
  });
})();